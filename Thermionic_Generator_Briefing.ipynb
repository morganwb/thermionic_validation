{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Nano-Boxx Briefing\n",
    "\n",
    "Morgan Baxter, morgan.baxter@blacklynx.tech\n",
    "\n",
    "## Introduction\n",
    "\n",
    "The Nano-Boxx is a power source proposed by Birmingham Technologies to power their miniature air-sampling sensors. It is a type of thermionic generator that utilizes a nanofluid, a fluid containing nanoparticles, to produce a current. This notebook seeks to explain the physical phenomena behind the generator, as well as the limiting factors in its operation. The basis of a thermoionic generator is similar to the the operation of most batteries, where the movement of electrons is induced between a cathode and an anode, causing current to flow, in the opposite direction through a circuit, which can be utilized to do work. The key difference is that in thermionic generators, the energy driving the electrons comes from the temperature of the cathode, which releases electrons due to energy from atomic vibration. In this briefing, reference will be made to \"the article\" and to \"the patent\". These refer to \"Printed Self-Powered Miniature Air Sampling Sensors\", published in Sensors & Transducers, Vol.214, Issue 7, July 2017, and the US patent 2015/0229013 A1, filed Feb. 13, 2014, respectively."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div>\n",
    "<img src=\"https://i.imgur.com/CSD2X3z.png\"width=\"600\"/>\n",
    "</div>\n",
    "\n",
    "Figure 1: Outline of a thermionic generator from Birmingham patent application US 2015/0229013"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Technical Explanation\n",
    "\n",
    "The principle behind this thermionic generator is the release of electrons by a cathode, stimulated by thermal energy. The cathode, partially coated by cesium oxide, releases electrons through a nanofluid with a slightly lower work function, acting as a go-between to the anode. This allows for the electrons to travel to the anode, with an even slightly lower work function, around a circuit and back to the cathode. The collection of negative charge on the anode creates a potential difference and generates a current when the circuit is closed. This would seem to violate the laws of thermodynamics, as we are getting energy for free from the surrounding environment. However, we can show that the second law is not violated as explained in the article. To test whether this process is thermodynamically favored, we need to show that it results in a decrease in entropy. We can use the second law of thermodynamics, which relates an infinitesimal change in entropy $S$ with an infinitesimal change in heat $Q$:\n",
    "\n",
    "$$dS = \\frac{dQ}{T}$$\n",
    "\n",
    "If $dS \\geq 0$, then the process is thermodynamically favored.\n",
    "\n",
    "The cathode is coated with a cesium oxide that has some of the lowest electronegativity of any material, and therefore liberation of electrons does not require a large amount of energy. In theory, this energy could be liberated by the kinetic energy imparted on it by air molecules at room temperature, although it clearly occurs at higher temperatures. The key thing to note here is that the release of electrons by the cathode results in a very slight decrease in cathode temperature.  Therefore we can say that $T_e \\leq T_c$. Furthermore, we know that the work function of the cathode is greater than that of the anode, so we can say that $\\phi_e \\geq \\phi_c$. Here, we use the subscripts $e$ and $c$ to be short for emitter and collector, where the cathode emits the electrons and the anode collects them. The key equation in the article that addresses the question of thermodynamic favorability is included below.\n",
    "\n",
    "$$dS = \\frac{dQ}{T} \\rightarrow \\frac{Q_{in}}{T_e}-\\frac{Q_{out}}{T_c} \\rightarrow \\frac{J_e}{e} \\left ( \\frac{\\phi_e}{T_e} -\\frac{\\phi_c}{T_c}\\right) $$\n",
    "\n",
    "Since $T_e \\leq T_c$ and $\\phi_e \\geq \\phi_c$, we know that $dS \\geq 0$, and therefore the second law is satisfied. This logic is sound, but a few notes on this series of arguments are in order. First, it is worth noting that is it not the case that $\\phi_e \\geq \\phi_c$, but rather that $\\phi_e > \\phi_c$. This is just a minor detail. It is also worth noting that I have copied this \"equation\" as it is included in the article. Equation 11 does not include the equals signs that are required for a more rigorous proof. The key problem with the above argument stems from the dimensional analysis. The work function ($\\phi$)is given in electron-volts (energy), and entropy ($S$) is in units of joules per kelvin, temperature ($T$) is in kelvin, and heat ($Q$) is in joules. If we want to say that that $dS = \\frac{J_e}{e} \\left ( \\frac{\\phi_e}{T_e} -\\frac{\\phi_c}{T_c}\\right)$, then $\\frac{J_e}{e}$ has to be dimensionless. From inspection of equations (12) and (13), $j$ has units of Amperes per meters squared. In order for $\\frac{J_e}{e}$  to be dimensionless, this means that $e$ must also have units of current per area. Clarification about what $e$ stands for and general clarification of equation (11) is reccomended.\n",
    "\n",
    "Where it does state in the article that this is given in \"per electron\" units, where $e$ likely represents \"per electron\", a more rigerous form of equation 11 is desirable. We want to clearly show that for this process, $\\frac{dS}{dt} > 0$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Feasibility Investigation\n",
    "\n",
    "Energy density compared to other sources is shown below. The original chart is from Wikipedia: https://en.wikipedia.org/wiki/Energy_density#/media/File:Energy_density.svg The claimed energy per volume and energy per mass provided in the Sensors and Transducers paper is 1550 watt-hour per liter and 2000 watt-hour per kilogram. These correspond to 5.58 MJ/l and 7.2 MJ/kg respectively, which puts the Nano-Boxx at the red point in the chart. If these energy density claims are correct, the Nano-Boxx presents a clear advantage over traditional lithium-ion batteries. Lithium-air batteries can provide comparable energy density, but require recharging, which the Nano-Boxx does not.\n",
    "\n",
    "<div>\n",
    "<img src=\"https://i.imgur.com/Kf5srHm.png\"width=\"600\"/>\n",
    "</div>\n",
    "\n",
    "It is worth noting that there are significantly different values for energy per volume and mass in the patent. In the patent, the the prototype is given as having energy densities of 120 watt-hour per liter and 120 watt-hour per kilogram. This is considerably lower than than the values in the article. This discrepency is not noted in the article, which was published in 2017, while the patent was filed in 2014."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Questions/Comments for Birmingham Team \n",
    "\n",
    "\n",
    "1. Equations (12) and (13) give the currents from the first and second metals. You write that the absolute ambient temperature is $373^{\\circ} K$, which is equal to $100^{\\circ} C$. From equations (12) and (13), the current density $j$ is proportional to the temperature squared times $e$ to the power of the reciprocal of temperature, or $j \\propto T^2 e^{1/T}$. This means that the ratio between the current at $373^{\\circ} K$ and $323^{\\circ} K$, for example, is 75%, but between $373^{\\circ} K$ and $273^{\\circ} K$, is roughly 50%. They should provide information about characteristics of this cell at various operating regimes. Lithium-ion batteries suffer from significant performance degredation at low temperatures, as illustrated in the below graph. What are the current or voltage versus temperature characteristics of the cell?\n",
    "\n",
    "<div>\n",
    "<img src=\"https://blog2.optibike.com/wp-content/uploads/2009/12/lithium-battery-temperature-vs-capacity.jpg\"width=\"400\"/>\n",
    "</div>\n",
    "\n",
    "Figure 2: Battery performance characteristics of a Li-Ion battery used for bicycles\n",
    "\n",
    "2. The encyclopedia Britannica indicates that at high current draws, the oxide layer on the cathode is likely to degrade more rapidly: https://www.britannica.com/science/electricity/Electric-properties-of-matter#ref71579 Does this represent a problem that has been encountered and/or solved?\n",
    "\n",
    "3. Figure 6 in the Sensors and Transducers paper shows a potential difference of 4.7 volts across the anode and cathode. Has this been achieved in prototypes?\n",
    "\n",
    "4. Would it be safe to say that the reason this technology is now mature is because of the development of very low work function materials for use in the anode, as well as the development of nanofluid to act as an intermediate work function and to prevent the build-up of charge in the gap?\n",
    "\n",
    "5. In the patent, section $\\textbf{[0012]}$ says that the data in FIG 1 represents a heat sink at $300^{\\circ} K$. Does the heat sink refer to the cathode or the anode?\n",
    "\n",
    "6. There seems to be a discrepency between the patent and the sensors and transducers article. The patent states that \"$\\textbf{[0018]}$ The nanofluid CPD battery can generate useful amounts of electrical power converting thermal energy from a heat source only slightly above ambient room temperature air ( ~ $100^{\\circ} C$. above ambient)\". Meanwhile, the article states that \"the Nano-Boxx is a self-charging battery that generates electrons from ambient environments; the thermionic emossion battery produces power at any air temperature\". (page 4) This seems to contradict itself here; is the power at room temperature ($300^{\\circ} K$) \"useful\"? \n",
    "\n",
    "7. Would it be correct to say that the electrons are transferred between the cathode and anode strictly by the motion and collision of the nanoparticles due to brownian motion of the fluid they are suspended in? (Omitting the fact that electrons will move from the higher work function cathode into the nanofluid and from the nanofluid into the lower work function anode due to their respective work functions)\n",
    "\n",
    "8. What changes were made to build the Nano-Boxx that differ from the prototype nanofluid CPD battery array described in the patent that resulted in an order of magnitude increase in energy density?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
